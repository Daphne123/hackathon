import { Story } from './Story';
import { User } from "./User";
import { Message } from "./Message";

export interface Game{
    gameMaster: User;
    players : User[];
    storyGuessed: boolean;
    messages: Message[];
    story: Story;
}
export interface Message {
    isCorrect: boolean;
    message: String;
}
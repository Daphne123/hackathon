import { User } from "./User";
import { Game } from "./Game";

export interface Lobby{
    host: User;
    users : User[];
    lobbycode: number;
    game: Game
}
export interface Story {
    title: String;
    clue:String;
    story:String;
}
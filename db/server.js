const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const middlewares = jsonServer.defaults()
 
let portnr=3000;
server.use(middlewares)
server.use(router)
server.listen(portnr, () => {
  console.log('JSON Server is running on port: '+portnr)
})